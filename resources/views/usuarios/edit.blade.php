@extends('layouts.plantilla')
@section('title', 'Actualizar Usuairio')
@section('content')
    <h1>Actualizar Usuario - {{$usuario->name}}</h1>
    <br>
    <form class="form-horizontal" action="{{route('usuarios.update', $usuario)}}" method="POST" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
          <label for="name" class="col-sm-2 control-label">Nombre</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="name" name="name" placeholder="Nombre" value="{{old('name', $usuario->name)}}">
            @error('name')
                <br>
                <small>*{{$message}}</small>
            @enderror
          </div>
        </div>
        <div class="form-group">
          <label for="email" class="col-sm-2 control-label">Email</label>
          <div class="col-sm-10">
            <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{old('email', $usuario->email)}}">
            @error('email')
                <br>
                <small>*{{$message}}</small>
            @enderror
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Actualizar</button>
            <a href="{{url()->previous()}}" class="btn btn-danger" role="button">Cancelar</a>
          </div>
        </div>
      </form>
@endsection