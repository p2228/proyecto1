{{-- !-- Delete Warning Modal -->  --}}
<form action="{{ route('usuarios.destroy', $usuario->id) }}" method="post">
    <div class="modal-body">
        @csrf
        @method('DELETE')
        <h4 class="text-center">¿Esta seguro de borrar el registro {{ $usuario->name }} ?</h4>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-danger">Aceptar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
    </div>
</form>