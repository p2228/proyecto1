@extends('layouts.plantilla')
@section('title', 'Usuario: '.$usuario->name)
@section('content')
    <h1>Usuario: {{$usuario->name}}</h1>
    <h1>Email: {{$usuario->email}}</h1>
    <h1>Creado: {{$usuario->created_at}}</h1>
    <h1>Modificado: {{$usuario->updated_at}}</h1>
    <br>
    <a href="{{route('usuarios.index')}}" class="btn btn-danger" role="button">Volver a Usuarios</a>
@endsection