@extends('layouts.plantilla')
@section('title', 'Crear Usuairio')
@section('content')
    <h1>Creación de Usuario:</h1>
    <br>
    <form class="form-horizontal" action="{{route('usuarios.store')}}" method="POST">
        @csrf
        <div class="form-group">
          <label for="name" class="col-sm-2 control-label">Nombre</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="name" name="name" placeholder="Nombre" value="{{old('name')}}">
            @error('name')
                <br>
                <small>*{{$message}}</small>
            @enderror
          </div>
        </div>
        <div class="form-group">
          <label for="email" class="col-sm-2 control-label">Email</label>
          <div class="col-sm-10">
            <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{old('email')}}">
            @error('email')
                <br>
                <small>*{{$message}}</small>
            @enderror
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Guardar</button>
            <a href="{{url()->previous()}}" class="btn btn-danger" role="button">Cancelar</a>
          </div>
        </div>
      </form>
@endsection