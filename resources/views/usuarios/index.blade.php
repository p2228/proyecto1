@extends('layouts.plantilla')

@section('title', 'Usuarios')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Listado de Usuarios &nbsp; <a class="btn btn-success" href="{{ route('usuarios.create') }}" title="Crear Usuario"> <i class="fas fa-plus-circle"></i></a></h2>
            </div>
        </div>
    </div>
    
    <br>
    <table class="table table-hover">
        <thead class="thead-dark">
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Email</th>
            <th scope="col" colspan="2">Opciones</th>
        </thead>
        <tbody>
            @foreach ($usuarios as $usuario)
                <tr>
                    <td>{{$usuario->id}}</td>
                    <td><a href="{{route('usuarios.show', $usuario->id)}}">{{$usuario->name}}</a></td>
                    <td>{{$usuario->email}}</td>
                    <td>
                        <a href="{{ route('usuarios.edit', $usuario) }}"  title="Editar Usuario">
                            <i class="fas fa-edit fa-lg"></i>
                        </a>
                    </td>
                    <td>
                        <a data-toggle="modal" id="smallButton" data-target="#smallModal" data-attr="{{ route('usuarios.delete', $usuario) }}" title="Borrar Usuario">
                            <i class="fas fa-trash text-danger fa-lg"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{$usuarios->links()}}

    <div class="modal fade" id="smallModal" tabindex="-1" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>Mensaje</strong>:
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="smallBody">
                    <div>
                        <!-- the result to be displayed apply here -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        // display a modal (small modal)
        $(document).on('click', '#smallButton', function(event) {
            event.preventDefault();
            let href = $(this).attr('data-attr');
            $.ajax({
                url: href
                , beforeSend: function() {
                    $('#loader').show();
                },
                // return the result
                success: function(result) {
                    $('#smallModal').modal("show");
                    $('#smallBody').html(result).show();
                }
                , complete: function() {
                    $('#loader').hide();
                }
                , error: function(jqXHR, testStatus, error) {
                    console.log(error);
                    alert("Page " + href + " cannot open. Error:" + error);
                    $('#loader').hide();
                }
                , timeout: 8000
            })
        });
    
    </script>
@endsection