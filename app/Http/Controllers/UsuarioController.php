<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use Illuminate\Http\Request;
use App\Http\Requests\StoreUsuario;
use Illuminate\Support\Facades\Auth;

class UsuarioController extends Controller
{
    public function index(){
        
        if(Auth::check()){
            $usuarios = Usuario::orderBy('id', 'desc')->paginate(10);
            return view('usuarios.index', compact('usuarios'));
        }
  
        return redirect("login")->withErrors(['msg' => 'El usuario debe autenticarse.']);
    }

    public function create(){
        if(Auth::check()){
            return view('usuarios.create');
        }
        return redirect("login")->withErrors(['msg' => 'El usuario debe autenticarse.']);
    }

    public function store(StoreUsuario $request){
        $usuario = Usuario::create($request->all());
        return redirect()->route('usuarios.show', $usuario->id);
    }

    public function show($id){
        if(Auth::check()){
            $usuario = Usuario::find($id);
            return view('usuarios.show', ['usuario' => $usuario]);
        }
        return redirect("login")->withErrors(['msg' => 'El usuario debe autenticarse.']);
    }

    public function edit($id){
        if(Auth::check()){
            $usuario = Usuario::find($id);
            return view('usuarios.edit', compact('usuario'));
        }
        return redirect("login")->withErrors(['msg' => 'El usuario debe autenticarse.']);
    }

    public function update(StoreUsuario $request, Usuario $usuario){
        $usuario->update($request->all());
        return view('usuarios.show', ['usuario' => $usuario]);
    }

    public function delete($id){
        $usuario = Usuario::find($id);
        return view('usuarios.delete', compact('usuario'));
    }

    public function destroy(Usuario $usuario){
        $usuario->delete();
        return redirect()->route('usuarios.index');
    }
}
