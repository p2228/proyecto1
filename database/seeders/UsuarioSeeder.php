<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Usuario;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usuario = New Usuario();
        $usuario->name = "Hector Moreno";
        $usuario->email = "hectormoreno420@gmail.com";
        $usuario->password = Hash::make("123456");
        $usuario->email_verified_at = now();
        $usuario->remember_token = Str::random(10);
        $usuario->save();
    }
}
